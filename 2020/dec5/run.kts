import java.io.File

fun readFile(fileName: String): List<String> {
    return File(fileName).readLines()
}

fun find(str: String, range0: List<Int>, minC: Char, maxC: Char): Int {
    var range = range0.toMutableList()
    for (c in str) {
        val m = range[0] + (range[1] - range[0]) / 2
        if (c == minC) {
            range[1] = m
        } else if (c == maxC) {
            range[0] = m + 1
        }
    }
    if (range[0] != range[1]) {
        println("Something's wrong: $range")
    }
    return range[0]
}

fun seatNr(s: String): Int {
    val rr = listOf(0, 127)
    val cr = listOf(0, 7)
    val row = find(s.substring(0, 7), rr, 'F', 'B')
    val col = find(s.substring(7), cr, 'L', 'R')

    return row * 8 + col
}

fun seatNrs(strs: List<String>): List<Int> {
    return strs.map { seatNr(it) }
}

// Max seat ID = 127 * 8 + 7 = 1023
fun maxSeatNr(strs: List<String>): Int? {
    val seatNs = strs.map { seatNr(it) }
    return seatNs.maxOrNull()
}

fun emptySeat(strs: List<String>): Int {
    val seatNs = strs.map { seatNr(it) }.sorted()
    var seat = 0

    seatNs.forEachIndexed { i, s ->
        if (i < seatNs.size - 1 && seatNs[i + 1] != s + 1) {
            seat = s + 1
        }
    }
    return seat
}

// Part 1
// println(seatNrs(readFile("./testData")))
// 357
// println(maxSeatNr(readFile("./input")))
// 922

// Part 2
println(emptySeat(readFile("./input")))
// 747
