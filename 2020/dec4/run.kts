import java.io.File

fun readFile(fileName: String): List<String> {
    val rx = Regex("^$\n", RegexOption.MULTILINE)
    return File(fileName).readText().split(rx)
}

val mandatoryFields = listOf("ecl", "pid", "eyr", "hcl", "byr", "iyr", "hgt")
val optionalFields = listOf("cid")

fun heightValidation(v: String): Boolean {
    val f3 = v.take(3).toIntOrNull()
    val f2 = v.take(2).toIntOrNull()
    return  (v.endsWith("cm") && f3 != null && f3 >= 150 && f3 <= 193) ||
            (v.endsWith("in") && f2 != null && f2 >= 59 && f2 <= 76)
}

val rules = mapOf(
    "byr" to {v -> v.length == 4 && v.toInt() >= 1920 && v.toInt() <= 2002},
    "iyr" to {v -> v.length == 4 && v.toInt() >= 2010 && v.toInt() <= 2020},
    "eyr" to {v -> v.length == 4 && v.toInt() >= 2020 && v.toInt() <= 2030},
    "hgt" to ::heightValidation,
    "hcl" to {v -> v.matches(Regex("#([0-9]|[a-f]){6}"))},
    "ecl" to {v -> v.matches(Regex("amb|blu|brn|gry|grn|hzl|oth"))},
    "pid" to {v -> v.matches(Regex("[0-9]{9}"))},
    "cid" to {_ -> true}
)

fun hasMandaroryFields(pp: String): Boolean {
    val fields = pp.split(Regex("\\s")).map {
        it.split(":")[0]
    }
    return mandatoryFields.intersect(fields).size == mandatoryFields.size
}

fun validPassport(pp: String): Boolean {
    if (!hasMandaroryFields(pp)) return false

    val fields = pp.split(Regex("\\s")).map {
        it.split(":")
    }

    test@ for (f in fields) {
        if (f.size == 1) break@test
        val (key, value) = f
        val valid = rules[key]?.invoke(value)
        // println("$key, $value = $valid")
        if (valid != null && !valid) {
            return false
        }        
    }
    return true
}

fun validPassports(pps: List<String>): Int {
    var n = 0
    for (pp in pps) {
        if (validPassport(pp)) {
            n ++
        }
    }
    return n
}

// Part 1
println(hasMandaroryFields(readFile("./testData")))
// 2
println(hasMandaroryFields(readFile("./input")))
// 233

//  Part 2
println(validPassports(readFile("./input")))
// 111