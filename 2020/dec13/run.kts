import java.io.File

fun readFile(fileName: String): List<String> = 
    File(fileName).readLines()

fun fetchBus(timestamp: String, busLines: String): Int {
    val time = timestamp.toInt()
    val buses = busLines.split(",")
        .filter { it != "x" }
        .map { it.toInt() }
    val diffs = buses.map { (time / it + 1) * it - time }
    val min = diffs.minOrNull()
    val bestBus = buses[diffs.indexOf(min)]

    return bestBus * (min ?: 1)
}

fun findTimestamp(busLines: String): Int {
    // val buses = busLines.split(",")
    //     .filter { it != "x" }
    //     .map { it.toInt() }
    //     // .slice(IntRange(0, 1))

    // val minutes = busLines.split(",")
    //     .mapIndexed { index, el -> if (el == "x") el else "$index" }
    //     .filter { it != "x"}
    //     .map { it.toInt() }
    //     // .slice(IntRange(0, 1))

    val buses = listOf(17, 13, 19)
    val minutes = listOf(0, 2, 3)

    println(buses)
    println(minutes)

    IntRange(1, 13).forEach {
        println("(17 * $it) % 13 = ${(17 * it) % 13}")
    }

    IntRange(1, 19).forEach {
        println("(17 * $it) % 19 = ${(17 * it) % 19}")
    }

    // (17 * 7) % 13 = 2
    // (17 * 8) % 19 = 3
    println((17 * (7 + 13)) % 13)
    println((17 * (8 + 19)) % 19)

    listOf(1, 7, 8)


    // var c = 0
    // val x = buses[0]
    // var ts = 7
    // var r = listOf(1)

    // println(x)

    // 1068781
    // 100000000000000
    // while (r.sum() != 0) {
    //     ts = ts + x
    //     r = buses.mapIndexed { i, b -> (ts + minutes[i]) % b }
    //     // println("$ts: $r")
    //     // c ++
    // }

    // println(c)

    return 0
}



val input = readFile("./testData")
// val input = readFile("./input")

// Part 1

// fetchBus(input[0], input[1])
// 59
// 410


//  Part 2
findTimestamp(input[1])
// 