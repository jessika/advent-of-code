import java.io.File
import kotlin.math.abs

fun readFile(fileName: String): List<String> = 
    File(fileName).readLines()

val E = "E"
val W = "W"
val N = "N"
val S = "S"
val L = "L"
val R = "R"
val F = "F"

val turns = listOf(L, R)
val whs = listOf(N, S, E, W)

fun move(facing: String, line: String, travel: Int): Int {
    return when (Pair(facing, line)) {
        Pair(E, "EW") -> travel
        Pair(W, "EW") -> -travel
        Pair(N, "NS") -> travel
        Pair(S, "NS") -> -travel
        else -> 0
    }
}

// N, E, S, W
fun nextDir(dir: String, current: String): String {
    return when (Pair(dir, current)) {
        Pair(L, N) -> W
        Pair(L, E) -> N
        Pair(L, S) -> E
        Pair(L, W) -> S
        Pair(R, N) -> E
        Pair(R, E) -> S
        Pair(R, S) -> W
        else -> N
    }
}

fun turn(facing: String, direction: String, degrees: Int): String {
    var n = degrees / 90
    var nowFacing = facing
    while (n > 0) {
        nowFacing = nextDir(direction, nowFacing)
        n--
    }
    println("turn $n directions ($degrees) to $nowFacing")
    return nowFacing
}

fun sail(instructions: List<String>): Int {
    var facing = E
    var NS = 0
    var EW = 0

    instructions.forEach {
        val direction = it.substring(0, 1)
        val value = it.substring(1).toInt()
        // L/R change direction 
        if (turns.contains(direction)) {
            facing = turn(facing, direction, value)
        // Move forward 
        } else if (direction == F) {
            NS = NS + move(facing, "NS", value)
            EW = EW + move(facing, "EW", value)
        // Move in some direction...
        } else if (whs.contains(direction)) {
            NS = NS + move(direction, "NS", value)
            EW = EW + move(direction, "EW", value)            
        }
    }

    println("ending at (east: $EW, north: $NS)")
    return abs(NS) + abs(EW)
}

// Part 2

/* 
Clockwise:

(E, N)
(10, 4) -> (4, -10)
(4, -10) -> (-10, -4)
...

Counderclockwise:
(-10, -4) -> (4, -10)
*/ 


fun nextWaypoint(direction: String, waypoint: Pair<Int, Int>): Pair<Int, Int> {
    return when (direction) {
        R -> Pair(waypoint.second, - waypoint.first) // clockwise
        else -> Pair(-waypoint.second, waypoint.first)
    }
}

fun rotate(waypoint: Pair<Int, Int>, direction: String, degrees: Int): Pair<Int, Int> {
    var n = degrees / 90
    var newWaypoint = waypoint
    while (n > 0) {
        newWaypoint = nextWaypoint(direction, newWaypoint)
        n--
    }
    // println("turn $n directions ($degrees) to $newWaypoint")
    return newWaypoint
}

fun moveSteps(waypoint: Pair<Int, Int>, line: String, steps: Int): Int {
    return when (line) {
        "EW" -> waypoint.first * steps
        else -> waypoint.second * steps
    }
}

fun moveWaypoint(waypoint: Pair<Int, Int>, direction: String, distance: Int): Pair<Int, Int> {
    return when (direction) {
        E -> Pair(waypoint.first + distance, waypoint.second)
        W -> Pair(waypoint.first - distance, waypoint.second)
        N -> Pair(waypoint.first, waypoint.second + distance)
        else -> Pair(waypoint.first, waypoint.second - distance)
    }
}

fun sailAdvanced(instructions: List<String>): Int {
    var waypoint = Pair(10, 1)
    var EW = 0
    var NS = 0

    instructions.forEach {
        val direction = it.substring(0, 1)
        val value = it.substring(1).toInt()
        // L/R rotate the waypoint 
        if (turns.contains(direction)) {
            waypoint = rotate(waypoint, direction, value)
        // Move towards the waypoint
        } else if (direction == F) {
            EW = EW + moveSteps(waypoint, "EW", value)
            NS = NS + moveSteps(waypoint, "NS", value)
        // Move the waypoint
        } else if (whs.contains(direction)) {
            waypoint = moveWaypoint(waypoint, direction, value)
        }
    }

    println("ending at (east: $EW, north: $NS)")
    return abs(NS) + abs(EW)
}

// val instructions = readFile("./testData")
val instructions = readFile("./input")

// Part 1

// println(sail(instructions))
// 25
// 998

// Part 2
println(sailAdvanced(instructions))
// 286
// 71586