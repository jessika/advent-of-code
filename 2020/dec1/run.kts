import java.io.File

val report = mutableListOf(1721,979,366,299,675,1456)

fun readFile(name: String): List<Int> =
    File(name).readLines().map {
        it.toInt()
    }

fun findN(numbers: List<Int>, sum: Int, n: Int): Int {
    for (x in numbers) {
        val y = sum - x
        if (n > 2) {
            val sum2 = findN(numbers, y, n - 1)
            if (sum2 > 0) {
                return x * sum2
            }
        } else if (numbers.contains(y)) {
            return x * y
        }
    }
    return 0
}


val input = readFile("./dec1/input.txt")

// Part 1
println(findN(input, 2020, 2))
// 960075


// Part 2
println(findN(input, 2020, 3))
// 212900130
