import java.io.File
import kotlin.math.pow

val OCCUPIED = "#"
val FREE = "L"
val FLOOR = "."
var size = 10
val neighbours = listOf(
    Pair(-1, 1), Pair(-1, 0), Pair(-1, -1),
    Pair(0, -1), Pair(0, 1),
    Pair(1, -1), Pair(1, 0), Pair(1, 1)
)

fun readFile(fileName: String): List<List<String>> {
    val universe = File(fileName).readLines().map{ it.chunked(1) }
    println(universe.joinToString("\n"))
    size = universe[0].size
    return universe
}

fun withinBounds(seat: Pair<Int, Int>, n: Pair<Int, Int>): Boolean =
    seat.first + n.first > -1 && seat.first + n.first < universe.size &&
    seat.second + n.second > -1 && seat.second + n.second < size

fun occupied(universe: List<List<String>>, dot: Pair<Int, Int>): Int {
    return neighbours.fold(0) { sum, n ->
        sum + if (
            withinBounds(dot, n) &&
            universe[dot.first + n.first][dot.second + n.second] == OCCUPIED
        ) 1 else 0
    }
}

fun isOccupied(universe: List<List<String>>, dot: Pair<Int, Int>, n: Pair<Int, Int>): Boolean {
    return if (!withinBounds(dot, n)) false else {
        val otherDot = universe[dot.first + n.first][dot.second + n.second]
        if (otherDot == OCCUPIED) true
        else if (otherDot == FREE) false
        else isOccupied(universe, Pair(dot.first + n.first, dot.second + n.second), n)
    }
}

fun occupied2(universe: List<List<String>>, dot: Pair<Int, Int>): Int {
    return neighbours.fold(0) { sum, n ->
        sum + if (isOccupied(universe, dot, n)) 1 else 0
    }
}

fun sitDown(universe: List<List<String>>): Pair<Boolean, List<List<String>>> {
    var changes = false
    val nextState = universe.mapIndexed { i, row ->
        row.mapIndexed { j, el ->
            val no =  occupied2(universe, Pair(i, j))
            // println("($i, $j) occupied: $no")
            if (el == FREE) {
                if (no == 0) {
                    changes = true
                    OCCUPIED
                } else FREE
            } else if (el == OCCUPIED) {
                if (no > 4) {
                    changes = true
                    FREE
                } else OCCUPIED
            } else {
                el
            }
        }
    }
    return Pair(changes, nextState)
}

fun countOccupied(universe: List<List<String>>): Int {
    return universe.fold(0) { sum, el ->
        sum + el.filter { it == OCCUPIED }.size
    }
}

fun run(universe: List<List<String>>): Int {
    var count = 0
    var state = universe
    var changes = true
    while (changes) {
        val res = sitDown(state)
        changes = res.first
        state = res.second
        count ++
    }

    println("Stable after $count rounds!")
    return countOccupied(state)
}

// Part 1
val universe = readFile("./input")

// println(run(universe))
// 37
// println(run(universe))
// 2270

// Part 2
// println(run(universe))
// 26
println(run(universe))
// 2042

