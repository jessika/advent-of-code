import java.io.File

val testData = listOf(
    "1-3 a: abcde",
    "1-3 b: cdefg",
    "2-9 c: ccccccccc"
)

val rx = Regex("(\\d+)-(\\d+)\\s([a-z]):\\s([a-z]+)")

fun readFile(name: String): List<Int> =
    File(name).readLines().map {
        it.toInt()
    }

fun correctPwd(pwd: CharSequence, x: Int, y: Int, c: Char): Boolean {
    val count = pwd.count { it == c }
    return x <= count && count <= y
}

fun correctPwd2(pwd: CharSequence, m: Int, n: Int, c: Char): Boolean =
    pwd[x - 1] == c && pwd[y - 1] != c ||
    pwd[x - 1] != c && pwd[y - 1] == c

fun correctPwds(
    lines: List<String>,
    isCorrect: (CharSequence, Int, Int, Char) -> Boolean
): Int {
    var n = 0
    for (l in lines) {
        val matches = rx.find(l)
        if (matches != null) {
            val (_, x, y, c, pwd) = matches.groupValues
            if (isCorrect(pwd, x.toInt(), y.toInt(), c.single())) {
                n ++
            }
        }
    }
    return n
}

val lines = File("./input").readLines()

// Part 1
// println(correctPwd("abcde", 1, 3, 'b'))
// println(correctPws(testData))
println(correctPwds(lines, ::correctPwd))
// 548

// Part 2
println(correctPwds(lines, ::correctPwd2))
// 768