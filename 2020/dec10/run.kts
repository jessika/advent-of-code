import java.io.File
import kotlin.math.pow

fun readFile(fileName: String): List<Int> = 
    File(fileName).readLines().map { it.toInt() }

fun joltages(adapters: List<Int>): MutableList<Int> {
    var diffs: MutableList<Int> = mutableListOf(0, 0, 1)
    var joltage = 0

    adapters.sorted().forEach lit@{
        val diff = it - joltage
        if (diff > 3) {
            println("Ooops! $diff, $it, $joltage")
            return@lit
        }
        diffs[diff - 1] ++
        joltage = it
    }

    return diffs
}

// [1, 4, 5, 6, 7, 10, 11, 12, 15, 16, 19]
// [0, 3, 1, 1, 1, 3,  1,  1,  3,  1,  3]

fun arrangements(adapters: List<Int>): Int {
    val a = adapters.sorted()
    println(a)

    // note: can remove 1, 2, 3: but not all at the same time..
    a.forEachIndexed lit@{ i, it ->
        if (a[i + 1] - it == 3) { // no alternatives
        } else if (a[i + 1] - it == 2) {
            if (a[i + 2] - it == 3)
        }
    }

    // sorted.forEachIndexed lit@{ index, it ->
    //     if (index - 1 >= 0 && index + 1 <= m) {
    //         if (sorted[index + 1] - sorted[index -1] <= 3) {
    //             removeable ++
    //             println("Can remove: $it [${sorted[index - 1]}, $it, ${sorted[index + 1]}]")
    //         }
    //     }
    // }

    println(removeable)

    return 2.toDouble().pow(removeable).toInt()
}


// Part 1

// println(joltages(readFile("./testData")))
// [7, 0, 5]
// println(joltages(readFile("./input")))
// 2475

// Part 2

// println(readFile("./testData").sorted())
println(arrangements(readFile("./testData")))
// 8
println(arrangements(readFile("./testData2")))
// 
// 19208