import java.io.File

fun readFile(fileName: String): List<String> {
    val rx = Regex("^$\n", RegexOption.MULTILINE)
    return File(fileName).readText().split(rx)
}


fun counts(str: List<String>): Int {
    val uniq = str.map { s -> 
        s.groupBy { it }.size - 1
    }

    return uniq.sum()
}

fun allAnswered(s: String): Int {
    val l = s.split("\n").size - 1
    val g = s.groupBy { it } 

    return g.filter { (_, v) -> v.size == l }.values.size
}

fun countAll(str: List<String>): Int {
    val uniq = str.map { s ->
        allAnswered(s) - 1
    }

    return uniq.sum()
}

// Part 1
// println(counts(readFile("./testData")))
// 11
// println(counts(readFile("./input")))
// 6542

// Part 2
// println(countAll(readFile("./testData")))
// 6
println(countAll(readFile("./input")))
// 3299
