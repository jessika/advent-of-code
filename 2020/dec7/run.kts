import java.io.File

fun readFile(fileName: String): List<String> {
    return File(fileName).readLines()
}

val rx1 = Regex("(?<=\\d)\\s")
val rx2 = Regex("\\sbag")

fun doMagic(rules: MutableList<List<String>>, color: String, ti: Int): MutableList<String> {
    var canContain: MutableList<String> = mutableListOf()

    rules.forEach { rule ->
        val (newColor, _, bagColor) = rule
        if (bagColor == color) {
            println("${"-".repeat(ti)} $newColor => $color")
            canContain.add(newColor)
            canContain.addAll(doMagic(rules, newColor, ti + 2))
        }
    }

    return canContain
}

fun parse(lines: List<String>, c: String): MutableList<List<String>> {    
    var rules: MutableList<List<String>> = mutableListOf()

    lines.forEach outer@{
        val (color, rest) = it.split(" bags contain ")
        val right = rest.split(", ")
        if (color == c) { // ignore these
            // println("Ignoring... $color")
            return@outer
        }
        // println("$color: $right")
        right.forEach {
            if (it == "no other bags.") {  // ignore these
                // println("Ignoring... $color")
                return@outer
            }
            val (n, rest1) = it.split(rx1)
            val (bagColor, _) = rest1.split(rx2)
            rules.add(listOf(color, n, bagColor))
        }
    }
    return rules
}

fun getBags(rules: MutableList<List<String>>, c: String): Int {
    var canContain: MutableList<String> = mutableListOf()

    rules.forEach {
        val (color, _, bagColor) = it
        if (bagColor == c) { // !canContain.contains(bagColor)
            canContain.add(color)
            println("$color => $c")
            canContain.addAll(doMagic(rules, color, 2)) 
        }
    }
    // println("\n\nContaining: $canContain")
    println("Distinct: ${canContain.distinct().sorted().joinToString("\n")}")
 
    return canContain.distinct().size
}

// Part 1
// println(parse(readFile("./testData"), "shiny gold"))
// 4

// val rules = parse(readFile("./input"), "shiny gold")
// println(getBags(rules, "shiny gold"))
// 326

fun parse2(lines: List<String>): MutableList<List<String>> {    
    var rules: MutableList<List<String>> = mutableListOf()

    lines.forEach outer@{
        val (color, rest) = it.split(" bags contain ")
        val right = rest.split(", ")
        right.forEach {
            if (it == "no other bags.") {  // ignore these
                rules.add(listOf(color, "0", ""))
                return@outer
            }
            val (n, rest1) = it.split(rx1)
            val (bagColor, _) = rest1.split(rx2)
            rules.add(listOf(color, n, bagColor))
        }
    }
    return rules
}

fun getBags2(rules: MutableList<List<String>>, c: String): Int {
    var count = 0

    rules.forEach {
        val (color, n, bagColor) = it
        if (color == c) {
            count = count + n.toInt() + (n.toInt() * getBags2(rules, bagColor))
        }
    }
    return count
}


// Part 2
// val rules = parse2(readFile("./testData"))
// println(getBags2(rules, "shiny gold"))
// 32
val rules = parse2(readFile("./input"))
println(getBags2(rules, "shiny gold"))
// 5635
