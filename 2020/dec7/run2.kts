import java.io.File

fun readFile(fileName: String): List<String> {
    return File(fileName).readLines()
}

class Node(value: String?) {
    val value = value
    var parent: Node? = null
    var children: MutableList<Node> = mutableListOf()

    fun addChild(node: Node) {
        children.add(node)
        node.parent = this
    }

    fun find(value: String): Node? {
        if (this.value == value) {
            return this
        } else {
            for (c in children) {
                return c.find(value)
            }
            return null
        }
    }

    fun findAll(value: String): List<Node> {
        var nodes: MutableList<Node> = mutableListOf()
        var n: Node? = this
        while (n != null) {
            var found = n.find(value)
            if (found != null) {
                nodes.add(found)
            }
            n = if (found?.children?.size != null ) found.children[0] else null
        }
        return nodes
    }


    override fun toString(): String {
        var s = "${value}"
        if (!children.isEmpty()) {
            s += " {" + children.map { it.toString() } + " }"
        }
        return s
    }
}

val rx = Regex("\\d+\\s")

fun buildTree(lines: List<String>): Node {
    val tree = Node(null)

    lines.forEach {
        val (color, rules) = it.split(" bags contain ")
        var node = tree.find(color)
        var parent = if (node == null) Node(color) else node

        if (node == null) {
            tree.addChild(parent)
        }

        rules.split(", ").forEach { rule ->
            val value = if (rule == "no other bags.") null else rule.split(rx)[1].split(" bag")[0]
            parent.addChild(Node(value))
        }
    }
    return tree
}

fun parseTree(tree: Node, value: String): Int {
    var count = 0
    var nodes: List<Node> = tree.findAll(value)
    println(nodes)

    for (n in nodes) {
        var node: Node? = n
        while (node?.parent != null) {
            count ++
            node = node.parent
        }
    }

    return count    
}

// Part 1
val t = buildTree(readFile("./testData"))
println(t)
println(parseTree(t, "shiny gold"))
//  