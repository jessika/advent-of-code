import java.io.File
import java.math.BigInteger
import kotlin.math.pow

val x = 4294967296.toBigInteger()

fun readFile(fileName: String): List<String> = 
    File(fileName).readLines()

fun bin(dec: String): String =
    dec.toBigInteger().toString(2)

fun unsigned(v: BigInteger): BigInteger =
    v + x

// or 1 (rest 0)
// and 0 (rest 1)
fun save(value: String, masks: Pair<String, String>): BigInteger {
    val zeroMask = masks.first //.takeLast(value.length)
    val oneMask  = masks.second // .takeLast(value.length)
    val bin = unsigned(value.toBigInteger(2))
    val zeros = unsigned(zeroMask.toBigInteger(2))
    val ones = unsigned(oneMask.toBigInteger(2))
    // val bin = Integer.parseUnsignedInt(value, 2)
    // val zeros = Integer.parseUnsignedInt(zeroMask, 2)
    // val ones = Integer.parseUnsignedInt(oneMask, 2)
    println(bin)
    println("    $bin")
    println("and $zeroMask ")
    println("or  $oneMask")
    val added = bin and zeros or ones
    // println("    ${added.toString(2)} ($added)")
    return added
}

fun getMasks(mask: String): Pair<String, String> {
    val zero = mask.map { if (it == '0') '0' else '1' }.joinToString("")
    val one  = mask.map { if (it == '1') '1' else '0' }.joinToString("")
    return Pair(zero, one)
}

fun run(input: List<String>) {
    val mem: MutableMap<Int, Int> = mutableMapOf()
    var masks = Pair("", "")

    input.forEach {
        if (it.startsWith("mask")) {
            masks = getMasks(it.split("mask = ")[1])
        } else {
            val (_, i, _, v) = it.split("[", "]", "= ")
            val b = bin(v)
            val index = unsigned(i.toBigInteger()).toInt()
    
            val res = save(b, masks)
            println("index $i => $v (${res.toInt()})")
    
            mem[index] = res.toInt()
        }
    }

    println(mem.values.sum())
}

// val input = readFile("./testData")
val input = readFile("./input")

// Part 1
run(input)
// 165
// 332393515 => too low...