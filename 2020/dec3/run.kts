import java.io.File
import java.math.BigInteger

val testData = listOf(
    "..##.......",  // 12 -> 2
    "#...#...#..",
    ".#....#..#.",
    "..#.#...#.#",
    ".#...##..#.",
    "..#.##.....",
    ".#.#.#....#",
    ".#........#",
    "#.##...#...",
    "#...##....#",
    ".#..#...#.#",
)

fun readFile(name: String): List<String> =
    File(name).readLines()

fun isTree(point: Char): Boolean =
    point == '#'

fun nTrees(lines: List<String>, dX: Int, dY: Int): Int {
    var trees = 0
    var y = 0
    var x = 0
    val max = lines[0].length - 1

    while (y < lines.size - dY) {
        y = y + dY
        x = x + dX
        if (x > max) {
            x = x - max - 1
        }
        if (isTree(lines[y][x])) {
            trees ++
        }
    }

    return trees
}

fun testSlopes(lines: List<String>, slopes: List<Pair<Int, Int>>): BigInteger =
    BigInteger.valueOf(
        slopes.fold(1, { acc, p ->
            acc * nTrees(lines, p.first, p.second)
        })
    )

val lines = File("./input").readLines()

val slopes = listOf(
    Pair(1, 1),
    Pair(3, 1),
    Pair(5, 1),
    Pair(7, 1),
    Pair(1, 2)
)

// Part 1
println(nTrees(testData, 3, 1))
// 7
println(nTrees(lines, 3, 1))
// 211

// Part 2
println(testSlopes(testData, slopes))
// 336
println(testSlopes(lines, slopes))
// 3584591857