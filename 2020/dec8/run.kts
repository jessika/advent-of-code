import java.io.File

fun readFile(fileName: String): List<String> = 
    File(fileName).readLines()

fun run(lines: List<String>): Pair<Int, Int> {
    var acc = 0
    var lineNr = 0
    var readInstructions: MutableList<Int> = mutableListOf()

    while (lineNr < lines.size && !readInstructions.contains(lineNr)) {
        readInstructions.add(lineNr)
        var (instruction, value) = lines[lineNr].split(" ")

        when(instruction) {
            "acc" -> {
                acc = acc + value.toInt()
                lineNr = lineNr + 1
            }
            "jmp" -> lineNr = lineNr + value.toInt()
            else -> lineNr = lineNr + 1
        }
    }

    return Pair(acc, lineNr)
}

fun didTerminate(lines: List<String>, result: Pair<Int, Int>): Boolean =
    result.second >= lines.size

fun testSwitch(lines: List<String>) {
    lines.forEachIndexed { index, line ->
        var (instruction, value) = line.split(" ")
        if (instruction == "jmp") {
            val newLines = lines.mapIndexed { i, e ->
                if (index == i) "nop $value" else e
            }
            var result = run(newLines)
            if (didTerminate(lines, result)) {
                println("Success! ${result.first}")
                return
            }
        }
    }
}

// Part1
// println(run(readFile("./testData")))
// 5
println(run(readFile("./input")))
// 2003

// Part 2
// testSwitch(readFile("./testData"))
// 8
// testSwitch(readFile("./input"))
// 1984