import java.io.File
import java.math.BigInteger

fun readFile(fileName: String): List<BigInteger> = 
    File(fileName).readLines().map { BigInteger(it) }

fun getCombinations(numbers: MutableList<BigInteger>): MutableList<BigInteger> {
    return numbers.map { x ->
        numbers.map { y ->
            if (x != y) x.plus(y) else 0.toBigInteger()
        }
    }.flatten().toMutableList()
}

fun run(numbers: List<BigInteger>, size: Int): BigInteger? {
    var preamble: MutableList<BigInteger> = numbers.slice(IntRange(0, size - 1)).toMutableList()
    var options: MutableList<BigInteger> = getCombinations(preamble)
    var i = size

    while(options.contains(numbers[i])) {
        preamble.removeAt(0)
        preamble.add(numbers[i])
        options = getCombinations(preamble)
        i ++
    }

    return numbers[i]
}

fun findSum(numbers: List<BigInteger>, goal: BigInteger): BigInteger? {
    var end = 0
    var start = 0
    var sum = 0.toBigInteger()
    
    while (sum != goal) {
        sum = sum.plus(numbers[end])
        if (sum > goal) {
            start ++
            sum = numbers[start]
            end = start + 1
        } else if (sum < goal) {
            end ++
        }
    }

    val nrs = numbers.slice(IntRange(start, end)).sorted()

    return nrs[0].plus(nrs[nrs.size - 1])
}

// Part 1
// println(run(readFile("./testData"), 5))
// 127
// println(run(readFile("./input"), 25))
// 21806024

// Part 2
// println(findSum(readFile("./testData"), 127.toBigInteger()))
// 62
println(findSum(readFile("./input"), 21806024.toBigInteger()))
// 2986195