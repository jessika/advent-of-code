PROGRAM-ID. rock-paper-scissors.

ENVIRONMENT DIVISION.
INPUT-OUTPUT SECTION.
FILE-CONTROL.
       SELECT Inputfile ASSIGN TO WS-FILE-NAME
       ORGANIZATION IS LINE SEQUENTIAL.
DATA DIVISION.
FILE SECTION.
FD Inputfile.
01 FileRecord.
  02 F-OPPONENT   PIC A(1).
  02 FILLER       PIC X(1).
  02 F-RESPONSE   PIC A(1).

WORKING-STORAGE SECTION.
01 WS-FILE-NAME   PIC X(20).

LOCAL-STORAGE SECTION.
01 END-OF-FILE    PIC Z(1) VALUE IS ZERO.
01 OPPONENT       PIC 9(1).
01 RESPONSE       PIC 9(1).
01 RESULT-SUM     PIC 9(6).

*> 01 WIN-POINTS PIC 9 VALUE 6.
*> 01 DRAW-POINTS PIC 9 VALUE 3.
*> 01 LOSE-POINTS PIC 9 VALUE 0.

01 LS-TABLE.
  02 LS-ROWS  OCCURS 3 TIMES INDEXED BY O.
    03 FILLER   PIC A(3).
    03 LS-COLS  OCCURS 3 TIMES INDEXED BY R.
      04 POINTS   PIC 9.

*> Part one
*>    X     Y     Z
*> A  3+1   6+2   0+3
*> B  0+1   3+2   6+3
*> C  6+1   0+2   3+3

*> Part two
*>    X     Y     Z
*> A  0+3   3+1   6+2
*> B  0+4   3+2   6+3
*> C  0+2   3+3   6+1

*> 012535
*> 015457

LINKAGE SECTION.
01 L-FILE-NAME      PIC X(20).
01 L-RESULT-1       PIC 9(10).
01 L-RESULT-2       PIC 9(10).

PROCEDURE DIVISION USING L-FILE-NAME, L-RESULT-1, L-RESULT-2.

MAIN.
  MOVE " A 483 B 159 C 726" TO LS-TABLE
  MOVE L-FILE-NAME TO WS-FILE-NAME.
  OPEN INPUT Inputfile.
  PERFORM PART-ONE UNTIL END-OF-FILE = 1.
  CLOSE Inputfile.
  MOVE RESULT-SUM TO L-RESULT-1.
  PERFORM DISPLAY-RESULT.

  MOVE " A 348 B 159 C 267" TO LS-TABLE
  MOVE 0 TO END-OF-FILE.
  SET RESULT-SUM TO 0.
  OPEN INPUT Inputfile.
  PERFORM PART-ONE UNTIL END-OF-FILE = 1.
  CLOSE Inputfile.
  MOVE RESULT-SUM TO L-RESULT-2.
  PERFORM DISPLAY-RESULT.

  EXIT PROGRAM.

TRANSLATE-OPPONENT.
EVALUATE F-OPPONENT
  WHEN "A"
   MOVE 1 TO OPPONENT
  WHEN "B"
    MOVE 2 TO OPPONENT
  WHEN "C"
    MOVE 3 TO OPPONENT
END-EVALUATE.

TRANSLATE-RESPONSE.
EVALUATE F-RESPONSE
  WHEN "X"
    MOVE 1 TO RESPONSE
  WHEN "Y"
    MOVE 2 TO RESPONSE
  WHEN "Z"
    MOVE 3 TO RESPONSE
END-EVALUATE.

PART-ONE.
READ Inputfile RECORD
  AT END
     MOVE 1 TO END-OF-FILE
  NOT AT END
    PERFORM TRANSLATE-OPPONENT
    PERFORM TRANSLATE-RESPONSE
    ADD POINTS(OPPONENT, RESPONSE) TO RESULT-SUM
END-READ.

DISPLAY-RESULT.
DISPLAY "Win points: " RESULT-SUM.
