PROGRAM-ID. climb.

ENVIRONMENT DIVISION.
INPUT-OUTPUT SECTION.
FILE-CONTROL.
  SELECT Inputfile ASSIGN TO "testdata"
  ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
FILE SECTION.
FD Inputfile RECORD VARYING FROM 1 TO 150 DEPENDING ON WS-ROW-LENGTH.
01 F-Elevation      PIC A(150).

WORKING-STORAGE SECTION.
01 WS-FILE-NAME     PIC X(20).
01 WS-ROW-LENGTH    PIC 9(3) VALUE ZERO.

LOCAL-STORAGE SECTION.
01 END-OF-FILE      PIC Z(1) VALUE ZERO.

01 N-Rows           PIC 9(3) VALUE 1.
01 N-Cols           PIC 9(3) VALUE 1.

01 Map  OCCURS 1 TO 150 TIMES
        DEPENDING ON N-Rows
        INDEXED BY R1.
  05 Map-Cols OCCURS 8 TIMES INDEXED BY R2.
    10 ELevation PIC A.

01 Visited-Array OCCURS 1 TO 150 TIMES
                 DEPENDING ON N-Rows.
  05 Visited PIC X OCCURS 8 TIMES.

01 I PIC 9(3).
01 J PIC 9(3).

01 X PIC 9(3).
01 Y PIC 9(3).
01 X2 PIC 9(3).
01 Y2 PIC 9(3).

01 LS-CHARACTERS VALUE IS "abcdefghijklmnopqrstuvwxyz".
  05 LS-CHAR PIC A OCCURS 26 TIMES indexed by C1.

01 Goal.
  05 Goal-X PIC 9(3).
  05 Goal-Y PIC 9(3).

01 Is-Smaller PIC Z(1).
01 Is-Next PIC Z(1).

01 SEARCH-CHAR PIC A.
01 Current-Index PIC 99.
01 FOUND-INDEX PIC 99.
01 DIfference PIC S99.

01 Direction PIC X.
01 IS-DONE PIC Z(1).
01 N-Steps PIC 99.
01 Took-This-Many-Steps PIC 9(3).

PROCEDURE DIVISION.

MAIN-PROCEDURE.
  OPEN INPUT Inputfile.
  PERFORM READ-FILE UNTIL END-OF-FILE = 1.
  CLOSE Inputfile.
  PERFORM FIND-GOAL.
  DISPLAY "Goal at: (" Goal-X "," Goal-Y ")"
  PERFORM DISPLAY-MAP.
  SET X TO Goal-X
  SET Y TO Goal-Y
  MOVE "X" TO Visited(Goal-X, Goal-Y)
  PERFORM FIND-NEXT UNTIL IS-DONE = 1 OR N-Steps = 25.
  PERFORM DISPLAY-VISITED.
  DISPLAY "Took " Took-This-Many-Steps " steps"
  STOP RUN.

FIND-GOAL.
  PERFORM VARYING I FROM 1 BY 1 UNTIL I > N-Rows OR Goal-X > 0
    PERFORM VARYING J FROM 1 BY 1 UNTIL J > N-Cols OR Goal-Y > 0
      IF ELevation(I, J) = "E"
        SET Goal-X TO I
        SET Goal-Y TO J
      END-IF
    END-PERFORM
  END-PERFORM.

MOVE-LEFT.
  SET X2 to X
  COMPUTE Y2 = Y - 1.

MOVE-RIGHT.
  SET X2 to X
  COMPUTE Y2 = Y + 1.

MOVE-UP.
  COMPUTE X2 = X - 1
  SET Y2 to Y.

MOVE-DOWN.
  COMPUTE X2 = X + 1
  SET Y2 to Y.

FIND-NEXT.
  MOVE "^" TO Direction
  PERFORM MOVE-UP
  PERFORM EVALUATE-STEP
  MOVE "<" TO Direction
  PERFORM MOVE-LEFT
  PERFORM EVALUATE-STEP
  MOVE ">" TO Direction
  PERFORM MOVE-RIGHT
  PERFORM EVALUATE-STEP
  MOVE "v" TO Direction
  PERFORM MOVE-DOWN
  PERFORM EVALUATE-STEP
  COMPUTE N-Steps = N-Steps + 1.

EVALUATE-STEP.
  DISPLAY Visited(X2, Y2)
  IF Visited(X2, Y2) <> SPACE
    DISPLAY "Already been"
    NEXT SENTENCE
  END-IF

  EVALUATE X2 ALSO Y2
    WHEN ANY ALSO 0
    WHEN 0 ALSO ANY
    WHEN ANY ALSO 9
    WHEN 6 ALSO ANY
      NEXT SENTENCE
    WHEN 1 ALSO 1
      DISPLAY "SUCCESS!"
      MOVE 1 TO IS-DONE
    *> WHEN other
    *>   DISPLAY "..."
  END-EVALUATE

  DISPLAY "(" X ", " Y ") -> (" X2 ", " Y2 ") | " with no advancing
  DISPLAY Elevation(X, Y) " -> " Elevation(X2, Y2)

  INITIALIZE Current-Index, FOUND-INDEX
  *> elevation difference
  IF ELevation(X,Y) = 'E' *> end is z
    SET Current-Index TO 26
  ELSE 
    MOVE ELevation(X, Y) TO SEARCH-CHAR
    PERFORM GET-CHAR-INDEX
    SET Current-Index TO FOUND-INDEX
  END-IF

  IF ELevation(X2, Y2) = 'S'
    SET FOUND-INDEX TO 1 *> start is a
  ELSE
    MOVE ELevation(X2, Y2) TO SEARCH-CHAR
    PERFORM GET-CHAR-INDEX
  END-IF
  
  DISPLAY Current-Index " " FOUND-INDEX
  COMPUTE DIfference = FOUND-INDEX - Current-Index
  IF Current-Index = 0 or FOUND-INDEX = 0
    NEXT SENTENCE
  END-IF

  IF DIfference = -1 OR DIfference = 0
    DISPLAY "Success - one step down or even"
    MOVE Direction TO Visited(X, Y)
    SET X TO X2
    SET Y TO Y2
    COMPUTE Took-This-Many-Steps = Took-This-Many-Steps + 1
  ELSE IF FOUND-INDEX < Current-Index
    DISPLAY "Fail - too steep"
  ELSE
    DISPLAY "Success"
    MOVE Direction TO Visited(X, Y)
    SET X TO X2
    SET Y TO Y2
    COMPUTE Took-This-Many-Steps = Took-This-Many-Steps + 1
  END-IF.

READ-FILE.
  READ Inputfile
    AT END
       MOVE 1 TO END-OF-FILE
    NOT AT END
      *> PERFORM GET-PARAMS
      PERFORM PART-ONE
      ADD 1 TO N-Rows
  END-READ.

PART-ONE.
  IF N-Rows = 1
    SET N-Cols TO WS-ROW-LENGTH
    DISPLAY N-Cols " columns"
  END-IF
  *> STRING F-Elevation DELIMITED BY "." INTO Map(N-Rows)
  *> DISPLAY Map(N-Rows).
  MOVE F-Elevation TO Map(N-Rows).

GET-CHAR-INDEX.
  SET C1 TO 1
  SEARCH LS-CHAR
    AT END
      DISPLAY "NOT-FOUND: " SEARCH-CHAR
    WHEN SEARCH-CHAR = LS-CHAR(C1)
      SET FOUND-INDEX TO C1.

DISPLAY-MAP.
  DISPLAY "---------------------------------"
  PERFORM VARYING I FROM 1 BY 1 UNTIL I > N-Rows
    PERFORM VARYING J FROM 1 BY 1 UNTIL J > N-Cols
      DISPLAY ELevation(I, J) " " with no advancing
    END-PERFORM
    Display " "
  END-PERFORM
  DISPLAY "---------------------------------".

DISPLAY-VISITED.
  DISPLAY "---------------------------------"
  PERFORM VARYING I FROM 1 BY 1 UNTIL I > N-Rows
    PERFORM VARYING J FROM 1 BY 1 UNTIL J > N-Cols
      DISPLAY Visited(I, J) " " with no advancing
    END-PERFORM
    Display " "
  END-PERFORM
  DISPLAY "---------------------------------".
