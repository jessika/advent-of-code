PROGRAM-ID. run.

ENVIRONMENT DIVISION.
INPUT-OUTPUT SECTION.
FILE-CONTROL.
  SELECT Inputfile ASSIGN TO "input"
  ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
FILE SECTION.
FD Inputfile.
01 F-Row.
  05 F-Instruction  PIC X(4).
  05 FILLER         PIC X.
  05 F-Value        PIC X(3).

WORKING-STORAGE SECTION.
01 WS-FILE-NAME     PIC X(20).

LOCAL-STORAGE SECTION.
01 END-OF-FILE      PIC Z(1) VALUE ZERO.
01 V                PIC S99.

01 Cycle-Nr            PIC 9(3) value is 1.
01 Cycles              PIC S9(3) OCCURS 500 TIMES.
01 X-Value             PIC S9(3) value is 1.
01 Sum-Signal-Strength PIC S9(8).

01 N-Rows           PIC 99 VALUE IS 6.
01 N-Cols           PIC 99 VALUE IS 40.

01 Screen-Table.
  05 LS-Rows OCCURS 6 TIMES INDEXED BY R.
    10 LS-Cols OCCURS 40 TIMES INDEXED BY C.
      20 Pixel   PIC X VALUE ".".

01 Sprite-Pos       PIC 999 value is 1.
01 Distance         PIC S999.
01 I                PIC 999.
01 J                PIC 999.

PROCEDURE DIVISION.

MAIN-PROCEDURE.
  
  MOVE 1 TO Cycles(1)

  OPEN INPUT Inputfile.
  PERFORM READ-FILE UNTIL END-OF-FILE = 1.
  CLOSE Inputfile.

  *> DISPLAY 20  " " Cycles(20)
  *> DISPLAY 60  " " Cycles(60)
  *> DISPLAY 100 " " Cycles(100)
  *> DISPLAY 140 " " Cycles(140)
  *> DISPLAY 180 " " Cycles(180)
  *> DISPLAY 220 " " Cycles(220)

  *> 13140 (test)
  *> 13680
  *> DISPLAY Sum-Signal-Strength.

  PERFORM PART-TWO.
  *> PZGPKPEB
  PERFORM DISPLAY-SCREEN.
  STOP RUN.

READ-FILE.
  READ Inputfile
    AT END
       MOVE 1 TO END-OF-FILE
    NOT AT END
      PERFORM PART-ONE
  END-READ.

PART-TWO.
  PERFORM VARYING I FROM 1 BY 1 UNTIL I > N-Rows
    PERFORM VARYING J FROM 1 BY 1 UNTIL J > N-Cols
      COMPUTE Cycle-Nr = N-Cols * (I - 1) + J
      COMPUTE Sprite-Pos = Cycles(Cycle-Nr)
      SUBTRACT Sprite-Pos FROM J GIVING Distance
      IF Distance < 3 AND Distance >= 0
        MOVE "#" TO Pixel(I, J)
      END-IF
    END-PERFORM
  END-PERFORM.

PART-ONE.
    MOVE F-Value TO V.

    EVALUATE F-Instruction
      WHEN "addx"
        Add 1 TO Cycle-Nr
        SET Cycles(Cycle-Nr) TO X-Value
        Add 1 TO Cycle-Nr
        ADD V TO X-Value
        SET Cycles(Cycle-Nr) TO X-Value
      WHEN OTHER
        Add 1 TO Cycle-Nr
        SET Cycles(Cycle-Nr) TO X-Value
    END-EVALUATE

    COMPUTE Sum-Signal-Strength = 
      20  * Cycles(20)  +
      60  * Cycles(60)  +
      100 * Cycles(100) +
      140 * Cycles(140) +
      180 * Cycles(180) +
      220 * Cycles(220).

DISPLAY-SCREEN.
  PERFORM VARYING I FROM 1 BY 1 UNTIL I > N-Rows
    DISPLAY LS-Rows(I)
  END-PERFORM.
