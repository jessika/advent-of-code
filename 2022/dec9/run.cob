PROGRAM-ID. rope-tail.

ENVIRONMENT DIVISION.
INPUT-OUTPUT SECTION.
FILE-CONTROL.
  SELECT Inputfile ASSIGN TO "testdata"
  ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
FILE SECTION.
FD Inputfile.
01 F-Row.
  05 Direction  PIC A.
  05 FILLER     PIC X.
  05 F-Steps    PIC X(2).

WORKING-STORAGE SECTION.
01 WS-FILE-NAME     PIC X(20).

LOCAL-STORAGE SECTION.
01 END-OF-FILE      PIC Z(1) VALUE ZERO.

01 Board.
  05 LS-Rows OCCURS 5 TIMES INDEXED BY I.
    10 LS-Position OCCURS 6 TIMES INDEXED BY J.
      20 LS-Content PIC X value is "-".
      20 LS-Visited PIC X(1).

01 Steps        PIC 99.

01 N-Instructions PIC 9(3).
01 N-Rows PIC 9(4) value is 6.
01 N-Cols PIC 9(4) value is 5.

01 H-Row PIC 9(4) value is 5.
01 H-Col PIC 9(4) value is 1.
01 T-Row PIC 9(4) value is 5.
01 T-Col PIC 9(4) value is 1.
01 Total-Visited PIC 9(4) value is 1.

01 Tails PIC 9 occurs 10 times.

01 N-Tails PIC 9 value is 9.
01 Tail PIC 9 value is 1.

01 End-Row PIC 9.
01 End-Col PIC 9.

01 X PIC 9 value is 1.
01 Y PIC 99 value is 1.
01 Y-Disp PIC 9 value is 1.

01 K PIC 9 value is 1.

01 Start-Row PIC 9(4).
01 Start-Col PIC 9(4).
01 Step-Direction PIC S9.
01 Abs-distance PIC 99 value 0.
01 Tails OCCURS 9 TIMES.
  05 R PIC 9 value is 5.
  05 C PIC 9 value is 1.

PROCEDURE DIVISION. *> USING L-FILE-NAME, L-RESULT-1, L-RESULT-2.

MAIN-PROCEDURE.
  *> MOVE L-FILE-NAME TO WS-FILE-NAME.

  *> MOVE "H" TO LS-Content(H-Row,H-Col).
  *> PERFORM DISPLAY-BOARD.

  *> move 's' to LS-Content(H-Row, H-Col).
  OPEN INPUT Inputfile.
  PERFORM READ-FILE UNTIL END-OF-FILE = 1 OR N-Instructions = 2.
  CLOSE Inputfile.
  *> MOVE RESULT-1 TO L-RESULT-1.
  *> MOVE RESULT-2 TO L-RESULT-2.
  *> PERFORM DISPLAY-RESULT.

  *> Part one
  *> 6236
  display Total-Visited.
  STOP RUN.

READ-FILE.
  READ Inputfile
    AT END
       MOVE 1 TO END-OF-FILE
    NOT AT END
      *> display F-Row with no advancing
      move F-Steps to Steps
      display "====" Direction " " F-Steps "===="
      *> PERFORM PART-ONE
      PERFORM PART-TWO
      add 1 to N-Instructions
  END-READ.

PART-TWO.
   move "-" to LS-Content(H-Row, H-Col).
   *> move "-" to LS-Content(T-Row, T-Col).
   move H-Row to Start-Row
   move H-Col to Start-Col
   evaluate Direction
    when "R"
      add Steps TO H-Col
      move +1 to Step-Direction
      perform MOVE-TAIL-RIGHT-2
    when "L"
      subtract Steps from H-Col
      move -1 to Step-Direction
      perform MOVE-TAIL-LEFT-2
    when "D"
      add Steps TO H-Row
      move +1 to Step-Direction
      perform MOVE-TAIL-DOWN-2
    when "U"
      subtract Steps from H-Row
      move -1 to Step-Direction
      perform MOVE-TAIL-UP-2
  end-evaluate

  *> DISPLAY Direction " : " Steps " (" H-Row "," H-Col ")" WITH NO ADVANCING
  *> DISPLAY "(" T-Row "," T-Col ")"
  *> move "H" to LS-Content(H-Row, H-Col)
  perform DISPLAY-BOARD.

MOVE-TAIL-UP-2.
  compute End-Row = H-Row
  perform varying I from Start-Row by Step-Direction until I < End-Row
    DISPLAY  "H: (" I ", " H-Col ")"
    SET T-Row to I
    SET T-Col to H-Col
    MOVE "H" TO LS-Content(I, H-Col)
    perform varying Y from 1 by 1 until Y > N-Tails
      display " T" Y ": (" R(Y)   "," C(Y) "), T-Row " T-Row ", T-Col " T-Col
      compute Abs-distance = T-Row - R(Y)
      PERFORM TAIL-UP_DOWN-2
    end-perform
    PERFORM DISPLAY-BOARD
  end-perform.

MOVE-TAIL-DOWN-2.
  compute End-Row = H-Row
  perform varying I from Start-Row by Step-Direction until I > End-Col
    SET T-Row to I
    SET T-Col to H-Col
    MOVE "H" TO LS-Content(I, H-Col)
    perform varying Y from 1 by 1 until Y > N-Tails
      compute Abs-distance = T-Row - R(Y)
      PERFORM TAIL-UP_DOWN-2
    end-perform
    PERFORM DISPLAY-BOARD
  end-perform.

MOVE-TAIL-LEFT-2.
  compute End-Col = H-Col
  perform varying I from Start-Col by Step-Direction until I < End-Col
    SET T-Col to I
    MOVE "H" TO LS-Content(H-Row, I)
    perform varying Y from 1 by 1 until Y > N-Tails
      compute Abs-distance =  T-Col - C(Y)
      PERFORM TAIL-LEFT-RIGHT-2
    end-perform
    PERFORM DISPLAY-BOARD
  end-perform.

MOVE-TAIL-RIGHT-2.
  compute End-Col = H-Col
  *> MOVE "-" TO LS-Content(H-Row, H-Col)
  DISPLAY Start-Col " " Step-Direction " " End-Col
  perform varying I from Start-Col by Step-Direction until I > End-Col
    DISPLAY  "H: (" H-Row "," I  ")"
    SET T-Col to I
    MOVE "H" TO LS-Content(H-Row, I)
    perform varying Y from 1 by 1 until Y > N-Tails
      display " T" Y ": (" R(Y)   "," C(Y) "), T-Col " T-Col
      compute Abs-distance = T-Col - C(Y)
      PERFORM TAIL-LEFT-RIGHT-2
    end-perform
    PERFORM DISPLAY-BOARD
  end-perform.

TAIL-UP_DOWN-2.
  IF Abs-distance > 1
    compute T-Row = T-Row - 1 * Step-Direction
    compute Abs-distance = T-Col - C(Y)
    
    if Abs-distance > 1
      compute T-Col = T-Col + 1 * Step-Direction
    end-if

    set R(Y) TO T-Row
    set C(Y) TO T-Col

    move Y to Y-Disp
    display "MOVE T" Y-Disp " to (" T-Row "," C(Y) ")"
    move Y-Disp TO LS-Content(T-Row, C(Y))
    PERFORM CHECK-VISITED-2
  END-IF.
  
TAIL-LEFT-RIGHT-2.
  if Abs-distance > 1
    compute T-Col = T-Col - 1 * Step-Direction
    move Y to Y-Disp
    set R(Y) TO T-Row
    set C(Y) TO T-Col
    display "MOVE T" Y-Disp " to (" R(Y) "," T-Col ")"
    Move Y-Disp TO LS-Content(R(Y), T-Col)
    PERFORM CHECK-VISITED-2
  END-IF.


CHECK-VISITED-2.
  IF LS-Visited(T-Row, T-Col) = space AND Y = 9
    ADD 1 TO Total-Visited
  end-if.
  *> Move Y to LS-Visited(T-Row, T-Col).  










PART-ONE.  
   move "-" to LS-Content(H-Row, H-Col).
   move "-" to LS-Content(T-Row, T-Col).
   move H-Row to Start-Row
   move H-Col to Start-Col
   evaluate Direction
    when "R"
      add Steps TO H-Col
      move +1 to Step-Direction
      perform MOVE-TAIL-RIGHT
    when "L"
      subtract Steps from H-Col
      move -1 to Step-Direction
      perform MOVE-TAIL-LEFT
    when "D"
      add Steps TO H-Row
      move +1 to Step-Direction
      perform MOVE-TAIL-DOWN
    when "U"
      subtract Steps from H-Row
      move -1 to Step-Direction
      perform MOVE-TAIL-UP
  end-evaluate
  *> DISPLAY Direction " : " Steps " (" H-Row "," H-Col ")" WITH NO ADVANCING
  *> DISPLAY "(" T-Row "," T-Col ")"
  move "H" to LS-Content(H-Row, H-Col)
  move "T" to LS-Content(T-Row, T-Col)
  perform DISPLAY-BOARD.

MOVE-TAIL-UP.
  *> display "UP from " Start-Row " to " H-Row 
  perform varying I from Start-Row by Step-Direction until I < H-Row
    compute Abs-distance = I - T-Row
    *> move Distance to Abs-distance
    *> DISPLAY "Head to: (" I "," Start-Col ") ," WITH NO ADVANCING
    *> DISPLAY "Distance: " Abs-distance
    PERFORM TAIL-UP_DOWN
  end-perform.

MOVE-TAIL-DOWN.
  perform varying I from Start-Row by Step-Direction until I > H-Row
    compute Abs-distance = I - T-Row
    PERFORM TAIL-UP_DOWN
  end-perform.

MOVE-TAIL-LEFT.
  perform varying I from Start-Col by Step-Direction until I < H-Col
    compute Abs-distance = I - T-Col
    PERFORM TAIL-LEFT-RIGHT
  end-perform.

MOVE-TAIL-RIGHT.
  perform varying I from Start-Col by Step-Direction until I > H-Col
    compute Abs-distance = I - T-Col
    PERFORM TAIL-LEFT-RIGHT
  end-perform.

TAIL-UP_DOWN.
  if Abs-distance > 1
    compute T-Row = I - 1 * Step-Direction
    move H-Col TO T-Col
    PERFORM CHECK-VISITED
  END-IF.
  
TAIL-LEFT-RIGHT.
  if Abs-distance > 1
    compute T-Col = I - 1 * Step-Direction
    Move H-Row to T-Row
    PERFORM CHECK-VISITED
  END-IF.

CHECK-VISITED.
  IF LS-Visited(T-Row, T-Col) <> "#"
    ADD 1 TO Total-Visited
  Move "#" TO LS-Visited(T-Row, T-Col).  

DISPLAY-BOARD.
  display "                   "
  PERFORM VARYING K FROM 1 BY 1 UNTIL K > N-Rows
    DISPLAY LS-Rows(K)
  END-PERFORM.

*> DISPLAY-RESULT.
  *> MOVE RESULT-1 TO RESULT-DISPLAY-1
  *> MOVE RESULT-2 TO RESULT-DISPLAY-2
  *> DISPLAY "Part one: " RESULT-DISPLAY-1.
  *> DISPLAY "Part two: " RESULT-DISPLAY-2.
