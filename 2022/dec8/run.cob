PROGRAM-ID. tree-watching.

ENVIRONMENT DIVISION.
INPUT-OUTPUT SECTION.
FILE-CONTROL.
  SELECT Inputfile ASSIGN TO "input"
  ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
FILE SECTION.
FD Inputfile.
01 F-Row.
  05 F-Trees PIC 9 OCCURS 99 TIMES.

WORKING-STORAGE SECTION.
01 WS-FILE-NAME     PIC X(20).

LOCAL-STORAGE SECTION.
01 END-OF-FILE      PIC Z(1) VALUE ZERO.

01 Rows OCCURS 99 TIMES.
  05 Trees PIC 9 OCCURS 99.

01 N-Cols     PIC 99 value is 99.
01 N-Rows     PIC 99.
01 Max-Col    PIC 99.
01 Max-Row    PIC 99.

01 N-Visible  PIC 9(5).
01 Height     PIC 9.

01 Found-array OCCURS 99 times.
  05 Found    PIC 9 OCCURS 99 TIMES values are 0.

01 I          PIC 99.
01 J          PIC 99.
01 LS-Start   PIC 99.
01 R          PIC 99.
01 C          PIC 99.

01 Direction-Sight  PIC 9(8).
01 Tree-Sight       PIC 9(8).
01 Best-Sight       PIC 9(8).

01 RESULT-1         PIC 9(8).
01 RESULT-2         PIC 9(8).
01 RESULT-DISPLAY-1 PIC Z(8).
01 RESULT-DISPLAY-2 PIC Z(8).

*> LINKAGE SECTION.
*> 01 L-FILE-NAME      PIC X(20).
*> 01 L-RESULT-1       PIC 9(10).
*> 01 L-RESULT-2       PIC 9(10).

PROCEDURE DIVISION. *> USING L-FILE-NAME, L-RESULT-1, L-RESULT-2.

MAIN-PROCEDURE.
  *> MOVE L-FILE-NAME TO WS-FILE-NAME.
  OPEN INPUT Inputfile.
  PERFORM READ-FILE UNTIL END-OF-FILE = 1.
  CLOSE Inputfile.

  compute Max-Row = N-Rows - 1 
  compute Max-Col = N-Cols - 1 

  *> 1543
  PERFORM PART-ONE.
  COMPUTE RESULT-1 = N-Visible + (N-Rows - 2) * 2 + N-Cols * 2

  *> 595080
  PERFORM PART-TWO.
  MOVE Best-Sight TO RESULT-2

  *> MOVE RESULT-1 TO L-RESULT-1.
  *> MOVE RESULT-2 TO L-RESULT-2.
  PERFORM DISPLAY-RESULT.

  STOP RUN.

READ-FILE.
  READ Inputfile 
    AT END
       MOVE 1 TO END-OF-FILE
    NOT AT END
      ADD 1 TO N-Rows
      MOVE F-Row TO Rows(N-Rows)
  END-READ.

PART-TWO.
  PERFORM VARYING I FROM 1 BY 1 UNTIL I > N-Rows
    PERFORM VARYING J FROM 1 BY 1 UNTIL J > N-Cols
      SET Height to Trees(I, J)
      SET Tree-Sight TO 1
      PERFORM CHECK-SIGHT
    END-PERFORM
  END-PERFORM.

CHECk-SIGHT.
  SET Direction-Sight TO 0
  COMPUTE LS-Start = I + 1
  PERFORM VARYING R FROM LS-Start BY 1 UNTIL R > N-Rows
    ADD 1 TO Direction-Sight
    IF Trees(R, J) >= Height
      SET R to N-Rows
    END-IF
  END-PERFORM
  COMPUTE Tree-Sight = Tree-Sight * Direction-Sight
 
  COMPUTE LS-Start = I - 1
  SET Direction-Sight TO 0
  PERFORM VARYING R FROM LS-Start BY -1 UNTIL R < 1
    ADD 1 TO Direction-Sight
    IF Trees(R, J) >= Height
      SET R TO 1
    END-IF
  END-PERFORM
  COMPUTE Tree-Sight = Tree-Sight * Direction-Sight

  COMPUTE LS-Start = J + 1
  SET Direction-Sight TO 0
  PERFORM VARYING C FROM LS-Start BY 1 UNTIL C > N-Cols
    ADD 1 TO Direction-Sight
    IF Trees(I, C) >= Height
      SET C TO N-Cols
    END-IF
  END-PERFORM
  COMPUTE Tree-Sight = Tree-Sight * Direction-Sight

  SET Direction-Sight TO 0
  COMPUTE LS-Start = J - 1
  PERFORM VARYING C FROM LS-Start BY -1 UNTIL C < 1
    ADD 1 TO Direction-Sight
    IF Trees(I, C) >= Height
      SET C TO 1
    END-IF
  END-PERFORM
  COMPUTE Tree-Sight = Tree-Sight * Direction-Sight
  
  IF Tree-Sight > Best-Sight
    SET Best-Sight TO Tree-Sight
  END-IF.


PART-ONE.
  PERFORM VARYING I FROM 2 BY 1 UNTIL I = N-Rows
    SET Height TO Trees(I, 1)
    PERFORM VARYING J FROM 2 BY 1 UNTIL J = N-Cols
      PERFORM CHECK-TREE
    END-PERFORM

    SET Height TO Trees(I, N-Cols)
    PERFORM VARYING J FROM Max-Col BY -1 UNTIL J = 1
      PERFORM CHECK-TREE
    END-PERFORM
  END-PERFORM
  
  PERFORM VARYING I FROM 2 BY 1 UNTIL I = N-Cols
    SET Height TO Trees(1, I)
    PERFORM VARYING J FROM 2 BY 1 UNTIL J = N-Rows
      PERFORM CHECK-TREE-2
    END-PERFORM

    SET Height TO Trees(N-Rows, I)
    PERFORM VARYING J FROM Max-Row BY -1 UNTIL J = 1 
      PERFORM CHECK-TREE-2
    END-PERFORM
  END-PERFORM.

CHECK-TREE.
  IF Trees(I, J) > Height
    IF Found(I, J) = 0
      ADD 1 TO N-Visible
      SET FOUND(I, J) TO 1
    END-IF
    SET Height TO Trees(I, J)
  END-IF.

CHECK-TREE-2.
  IF Trees(J, I) > Height
    IF Found(J, I) = 0
      ADD 1 TO N-Visible
      SET FOUND(J, I) TO 1
    END-IF
    SET Height TO Trees(J, I)
  END-IF.

DISPLAY-RESULT.
  MOVE RESULT-1 TO RESULT-DISPLAY-1
  MOVE RESULT-2 TO RESULT-DISPLAY-2
  DISPLAY "Part one: " RESULT-DISPLAY-1
  DISPLAY "Part two: " RESULT-DISPLAY-2.
 