PROGRAM-ID. monkey-business.

ENVIRONMENT DIVISION.
INPUT-OUTPUT SECTION.
FILE-CONTROL.
  SELECT Inputfile ASSIGN TO "input-seba"
  ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
FILE SECTION.
FD Inputfile.
01 F-Row PIC X(80).

WORKING-STORAGE SECTION.
01 WS-FILE-NAME     PIC X(20).

LOCAL-STORAGE SECTION.
01 END-OF-FILE        PIC Z(1) VALUE ZERO.
01 IS-READING-HEADER  PIC 9 VALUE 1.
01 HEADER-RECORD.
  05 FILLER         PIC x(7).
  05 F-MONKEY-NR    PIC 9.

01 Monkey-Nr        PIC 9.
01 Instruction      PIC X(20).
01 F-Values         PIC X(60).

01 Operation-Rec.
  05 FILLER       PIC X(11).
  05 Op           PIC X(1).
  05 FILLER       PIC X(1).
  05 Op-Value     PIC X(3).

01 Division-Rec.
  05 FILLER       PIC X(14).
  05 Div-Value    PIC X(4).

01 Action-Rec.
  05 FILLER       PIC X(17).
  05 Monkey-Value PIC 9.

*> -----------------------
01 Monkeys    OCCURS 9 TIMES.
  05 Items          PIc 9(20) OCCURS 100 TIMES. *> < 40 items exist
  05 Divisor        PIC 9(20).
  05 Change-Value   PIC 9(20).
  05 Change-Op      PIC X.
  05 If-True        PIC 9.
  05 If-False       PIC 9.
  05 Inspected-Items PIC 9(8).

01 N-Monkeys      PIC 9.
01 N-Rounds       PIC 9(6) value is 100.

01 I              PIC 9(3). *> items
01 J              PIC 9(3). *> items
01 J2             PIC 9(3). *> items
01 M              PIC 9.    *> monkeys
01 M2             PIC 9.    *> monkeys
01 R              PIC 9(6). *> rounds

01 Is-Worry-Level-Decreasing PIC Z(1).
01 Worry-Level    PIC 9(20).
01 Trash          PIC 9(20).
01 Rem            PIC 9(20).
01 Toss-TO        PIC 9.
01 Common-Divisor PIC 9(20) value is 3.

01 RESULT           PIC 9(20).

PROCEDURE DIVISION.

MAIN-PROCEDURE.
  OPEN INPUT Inputfile.
  PERFORM READ-FILE UNTIL END-OF-FILE = 1.
  CLOSE Inputfile.

  SET N-Monkeys to Monkey-Nr.
  PERFORM DISPLAY-MONKEYS.

  *> Part 1: 347 * 342 = 118674
  *> MOVE 1 TO Is-Worry-Level-Decreasing.
  *> SET N-Rounds TO 20
  *> PERFORM PART-ONE.
  
  *> testdata 2713310158
  MOVE 0 TO Is-Worry-Level-Decreasing.
  SET N-Rounds TO 10000
  *> SET Common-Divisor TO 96577
  *> SET Common-Divisor TO 9699690
  SET Common-Divisor TO 9699690
  PERFORM PART-ONE.

  *> MONKEY 1: 00153007
  *> MONKEY 2: 00027035
  *> MONKEY 3: 00089792
  *> MONKEY 4: 00162012
  *> MONKEY 5: 00071968
  *> MONKEY 6: 00161890
  *> MONKEY 7: 00179940
  *> MONKEY 8: 00179690


  PERFORM GET-RESULT.
  PERFORM DISPLAY-MONKEYS.
  PERFORM DISPLAY-INSPECTED.
  *> Part 2: 32333418600
  DISPLAY "Result: " RESULT.

  STOP RUN.

PART-ONE.
  PERFORM VARYING R FROM 1 BY 1 UNTIL R > N-Rounds
    *> DISPLAY " "
    *> DISPLAY "=================== Round " R " ======================"
    *> DISPLAY " "
    PERFORM ROUND
    IF R = 1 OR R = 20 or R = 1000
      DISPLAY "--------- Round " R
      PERFORM DISPLAY-INSPECTED
    END-IF
  END-PERFORM.

ROUND.
  PERFORM VARYING M FROM 1 BY 1 UNTIL M > N-Monkeys
    *> DISPLAY "===== Monkey " M " turn "
    PERFORM VARYING I FROM 1 BY 1 UNTIL Items(M, I) = 0
      COMPUTE Inspected-Items(M) = Inspected-Items(M) + 1
      SET Worry-Level TO Items(M, I)
      
      IF Is-Worry-Level-Decreasing = 0
        DIVIDE Worry-Level By Common-Divisor GIVING Trash REMAINDER Worry-Level
      END-IF

      EVALUATE Change-Op(M) ALSO Change-Value(M)
        WHEN "*" ALSO 0
          COMPUTE Worry-Level = Worry-Level * Worry-Level
        WHEN "*" ALSO ANY
          COMPUTE Worry-Level = Worry-Level * Change-Value(M)
        WHEN "+" ALSO 0
          COMPUTE Worry-Level = Worry-Level * 2
        WHEN "+" ALSO ANY
          COMPUTE Worry-Level = Worry-Level + Change-Value(M)
        WHEN OTHER
          DISPLAY "Unknown computation"
      END-EVALUATE

      IF Is-Worry-Level-Decreasing = 1
       COMPUTE Worry-Level = Worry-Level / 3
      END-IF
      DIVIDE Worry-Level BY Divisor(M) GIVING Trash REMAINDER Rem
      *> DISPLAY "----------(" Worry-Level "/" Divisor(M) ") = " Trash ", Rem: " Rem

      EVALUATE Rem
        WHEN 0
          COMPUTE Toss-TO = If-True(M) + 1
        WHEN OTHER
          COMPUTE Toss-TO = If-False(M) + 1
      END-EVALUATE

      *> DISPLAY "Toss to monkey " Toss-TO ", worry level: " Worry-Level
      PERFORM TOSS
    END-PERFORM
    PERFORM REMOVE
  END-PERFORM.

REMOVE.
  PERFORM WITH TEST BEFORE VARYING J FROM 1 BY 1 UNTIL Items(M, J) = 0
    SET ITEMS(M, J) TO 0
  END-PERFORM.

TOSS.
  PERFORM VARYING J FROM 1 BY 1 UNTIL Items(Toss-TO, J) = 0
    continue
  END-PERFORM
  MOVE Worry-Level TO Items(Toss-TO, J).

GET-RESULT.
  SORT Monkeys on descending Inspected-Items
  COMPUTE RESULT = Inspected-Items(1) * Inspected-Items(2).

DISPLAY-MONKEYS.
  DISPLAY "---------------------------"
  PERFORM VARYING M2 FROM 1 BY 1 UNTIL M2 > N-Monkeys
    DISPLAY "MONKEY " M2 ":"
    PERFORM VARYING I FROM 1 BY 1 UNTIL Items(M2, I) = 0
      DISPLAY Items(M2, I)
    END-PERFORM
  END-PERFORM
  DISPLAY "---------------------------".

DISPLAY-INSPECTED.
  PERFORM VARYING M2 FROM 1 BY 1 UNTIL M2 > N-Monkeys
    DISPLAY "MONKEY " M2 ": " Inspected-Items(M2)
  END-PERFORM
  DISPLAY "---------------------------".

READ-FILE.
  READ Inputfile
    AT END
       MOVE 1 TO END-OF-FILE
    NOT AT END
      IF IS-READING-HEADER = 1
        PERFORM READ-HEADER
      ELSE
        PERFORM READ-CONTENT
  END-READ.

READ-HEADER.
  MOVE F-Row TO HEADER-RECORD
  COMPUTE Monkey-Nr = F-MONKEY-NR + 1
  MOVE 0 TO IS-READING-HEADER.

READ-CONTENT.
  UNSTRING F-Row DELIMITED BY ":"
    INTO Instruction, F-Values
  evaluate Instruction
    when "  Starting items"
      UNSTRING F-Values DELIMITED BY ", " INTO
        Items(Monkey-Nr, 1),
        Items(Monkey-Nr, 2),
        Items(Monkey-Nr, 3),
        Items(Monkey-Nr, 4),
        Items(Monkey-Nr, 5),
        Items(Monkey-Nr, 6),
        Items(Monkey-Nr, 7),
        Items(Monkey-Nr, 8)
    when "  Operation"
      MOVE F-Values TO Operation-Rec
      IF Op-Value <> "old"
        Set Change-Value(Monkey-Nr) TO Op-Value
      END-IF
      MOVE Op TO Change-Op(Monkey-Nr)
      MOVE Div-Value TO Divisor(Monkey-Nr)
    when "  Test"
      MOVE F-Values TO Division-Rec
      MOVE Div-Value TO Divisor(Monkey-Nr)
    when "    If true"
      MOVE F-Values TO Action-Rec
      MOVE Monkey-Value TO If-True(Monkey-Nr)
    when "    If false"
      MOVE F-Values TO Action-Rec
      MOVE Monkey-Value TO If-False(Monkey-Nr)
    when other
      continue
  end-evaluate  
  IF F-Row = SPACE
    MOVE 1 TO IS-READING-HEADER
  END-IF.
