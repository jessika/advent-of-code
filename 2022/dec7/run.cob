PROGRAM-ID. directories.

ENVIRONMENT DIVISION.
INPUT-OUTPUT SECTION.
FILE-CONTROL.
  SELECT Inputfile ASSIGN TO "input"
  ORGANIZATION IS LINE SEQUENTIAL.

DATA DIVISION.
FILE SECTION.
FD Inputfile.
01 F-Row            PIC X(80).

WORKING-STORAGE SECTION.
01 WS-FILE-NAME     PIC X(20).

LOCAL-STORAGE SECTION.
01 END-OF-FILE      PIC Z(1) VALUE ZERO.

01 LINE-COUNT       PIC 9(10) VALUE ZERO.

01 LS-First         PIC X(10).
01 LS-Second        PIC X(10).
01 LS-Third         PIC X(10).

01 CURRENT-DIR      PIC X(10).
01 CURRENT-PATH     PIC X(80).
01 PREVIOUS-DIR     PIC X(10).
01 PREVIOUS-PATH    PIC X(80).
01 TEMP-PATH        PIC x(80).

77 DIR              PIC X(10). *> search index
77 DIR-COUNT        PIC 9(10) VALUE IS 1. *> add index
77 FOUND-INDEX      PIC 9(10). *> set by search
01 I                PIC 9(10).
01 J                PIC 9(10).
01 N                PIC 9(10).
01 LAST-SLASH       PIC 9(10).

01 SIZES.
  05 DIRECTORY-ARRAY  OCCURS 1 TO 1000 TIMES
                      DEPENDING ON DIR-COUNT
                      INDEXED BY D1.
    10 TOTAL-FILE-SIZE  PIC 9(10).
    10 DIRECTORY        PIC X(10).
    10 DIRECTORY-ID     PIC 9(10).
    10 PATH             PIC X(80).

01 CURRENT-DIRECTORY-SIZE PIC 9(10).
01 FILE-SIZE              PIC 9(10).

01 DIR-TEMP           PIC X(10).
01 PATH-TEMP          PIC X(80).

01 PATH-NAME-LEN      PIC 9(4) VALUE IS 1.
01 PATH-NAME-LEN-TEMP PIC 9(4).

01 MAX-SIZE           PIC 9(10) VALUE IS 100000.

*> test stuff


01 RESULT-1         PIC 9(10) VALUE 0.
01 RESULT-2         PIC 9(10).
01 RESULT-DISPLAY-1 PIC Z(10).
01 RESULT-DISPLAY-2 PIC Z(10).

*> LINKAGE SECTION.
*> 01 L-FILE-NAME      PIC X(20).
*> 01 L-RESULT-1       PIC 9(10).
*> 01 L-RESULT-2       PIC 9(10).

*> Part 2
01 FILESYSTEM-SIZE  PIC 9(10) VALUE IS 70000000.
01 SPACE-NEEDED     pic 9(10) vALUE IS 30000000.
01 ROOT-SIZE        PIC 9(10).
01 UNUSED-SPACE     PIC 9(10).
01 FREEUP-SPACE    PIC 9(10).
01 CLOSEST-SIZE    PIC 9(10).
01 DIR-TO-DELETE   PIC X(80).

PROCEDURE DIVISION. *> USING L-FILE-NAME, L-RESULT-1, L-RESULT-2.

MAIN-PROCEDURE.
  *> MOVE L-FILE-NAME TO WS-FILE-NAME.
  *> MOVE RESULT-1 TO L-RESULT-1.
  *> MOVE RESULT-2 TO L-RESULT-2.

  OPEN INPUT Inputfile.
  PERFORM READ-FILE UNTIL END-OF-FILE = 1. *> OR LINE-COUNT > 50.
  CLOSE Inputfile.
  *> PERFORM DISPLAY-TABLE

  PERFORM ADD-TO-PARENT-DIRS
  PERFORM DISPLAY-TABLE

  *> testdata
  *> 95437

  *> part 1
  *> 1206825
  PERFORM PART-ONE

  *> part 2
  *> 9608311
  PERFORM PART-TWO

  PERFORM DISPLAY-RESULT
  STOP RUN.

PART-ONE.
  PERFORM VARYING I FROM 2 BY 1 UNTIL I > DIR-COUNT
    IF TOTAL-FILE-SIZE(I) <= MAX-SIZE
      COMPUTE RESULT-1 = RESULT-1 + TOTAL-FILE-SIZE(I)
    END-IF
  END-PERFORM.

PART-TWO.
  MOVE 48729145 TO ROOT-SIZE
  COMPUTE UNUSED-SPACE = FILESYSTEM-SIZE - ROOT-SIZE
  COMPUTE FREEUP-SPACE = SPACE-NEEDED - UNUSED-SPACE
  SET CLOSEST-SIZE TO FILESYSTEM-SIZE
  PERFORM VARYING I FROM 2 BY 1 UNTIL I > DIR-COUNT
    IF  TOTAL-FILE-SIZE(I) > FREEUP-SPACE AND
        TOTAL-FILE-SIZE(I) < CLOSEST-SIZE
          SET CLOSEST-SIZE TO TOTAL-FILE-SIZE(I)
    END-IF
  END-PERFORM
  MOVE CLOSEST-SIZE TO RESULT-2.

READ-FILE.
  READ Inputfile
    AT END
       PERFORM DO-ADD-TOTALS
       MOVE 1 TO END-OF-FILE
    NOT AT END
      ADD 1 TO LINE-COUNT
      PERFORM GET-PARAMS
  END-READ.

GET-PARAMS.
  UNSTRING F-Row DELIMITED BY ALL SPACES
  INTO LS-First, LS-Second, LS-Third
  IF LS-First = "$"
    PERFORM DO-COMMAND
  ELSE
    PERFORM DO-READ-CONTENT
  END-IF.

DO-COMMAND.
  IF LS-Second = "ls"
    CONTINUE
  ELSE IF LS-Second = "cd"

      *> add total or 
      MOVE CURRENT-PATH TO PATH-TEMP
      SET FOUND-INDEX TO 0
      PERFORM DO-SEARCH
      IF FOUND-INDEX = 0
        PERFORM DO-ADD-TOTALS
        ADD 1 TO DIR-COUNT
      END-IF

    IF LS-Third = ".."
      INITIALIZE TEMP-PATH, CURRENT-DIR

      PERFORM WITH TEST AFTER VARYING I FROM 80 BY -1 UNTIL I = 1
        IF CURRENT-PATH(I:1) = "/"
          SET LAST-SLASH TO I
          SET I TO 1
        END-IF
      END-PERFORM

      IF LAST-SLASH = 1
        INITIALIZE CURRENT-PATH
      ELSE 
        SUBTRACT 1 FROM LAST-SLASH
        MOVE CURRENT-PATH(1:LAST-SLASH) TO TEMP-PATH
        INITIALIZE CURRENT-PATH
        MOVE TEMP-PATH TO CURRENT-PATH
      END-IF
            
    ELSE
        MOVE LS-Third TO CURRENT-DIR

        IF CURRENT-DIR <> "/"
          STRING CURRENT-PATH DELIMITED BY SPACES,
                 "/",
                 CURRENT-DIR
                 INTO CURRENT-PATH
        ELSE
          MOVE "/" TO CURRENT-PATH
        END-IF
    END-IF *> which dir?
  END-IF.

DO-ADD-TOTALS.
  MOVE CURRENT-DIR TO DIRECTORY(DIR-COUNT)
  MOVE CURRENT-DIRECTORY-SIZE TO TOTAL-FILE-SIZE(DIR-COUNT)
  MOVE CURRENT-PATH TO PATH(DIR-COUNT)
  MOVE DIR-COUNT TO DIRECTORY-ID(DIR-COUNT)
  INITIALIZE CURRENT-DIRECTORY-SIZE.
  *> PERFORM DISPLAY-TABLE.

DO-READ-CONTENT.
  IF LS-First = "dir"
    CONTINUE
  ELSE
    MOVE LS-First TO FILE-SIZE
    ADD FILE-SIZE TO CURRENT-DIRECTORY-SIZE
    *> DISPLAY "Size: " FILE-SIZE " in " CURRENT-DIR " (total: " CURRENT-DIRECTORY-SIZE ")"
  END-IF.

ADD-TO-PARENT-DIRS.
  PERFORM VARYING I FROM 2 BY 1 UNTIL I > DIR-COUNT
    SET PATH-NAME-LEN TO 1
    INITIALIZE PATH-TEMP

    STRING PATH(I) DELIMITED BY SPACES
      INTO PATH-TEMP
      WITH POINTER PATH-NAME-LEN
    
    SUBTRACT 1 FROM PATH-NAME-LEN

    PERFORM VARYING J FROM 2 BY 1 UNTIL J > DIR-COUNT
      IF PATH(J)(1:PATH-NAME-LEN) = PATH-TEMP AND J <> I
        COMPUTE PATH-NAME-LEN-TEMP = PATH-NAME-LEN + 1
          IF PATH(J)(PATH-NAME-LEN-TEMP:1) = "/"
            *> DISPLAY "Adding " PATH(J) " to " PATH-TEMP
            ADD TOTAL-FILE-SIZE(J) TO TOTAL-FILE-SIZE(I)
          END-IF
      END-IF
    END-PERFORM

  END-PERFORM.
  
DO-SEARCH.
  SET D1 TO 1
  SEARCH DIRECTORY-ARRAY
    AT END
      CONTINUE
      *> DISPLAY "NOT-FOUND: " DIR
    WHEN PATH-TEMP = PATH(D1)
      COMPUTE FOUND-INDEX = DIRECTORY-ID(D1).
      *> DISPLAY "FILE SIZE: " TOTAL-FILE-SIZE(D1).

DISPLAY-TABLE.
  DISPLAY "---------------"
  PERFORM VARYING I FROM 1 BY 1 UNTIL I > DIR-COUNT 
    DISPLAY DIRECTORY-ID(I)     " | "
            DIRECTORY(I)        " | "
            PATH(I)             " | "
            TOTAL-FILE-SIZE(I)  " | "
  END-PERFORM
  DISPLAY "--------------".

DISPLAY-RESULT.
  MOVE RESULT-1 TO RESULT-DISPLAY-1
  MOVE RESULT-2 TO RESULT-DISPLAY-2
  DISPLAY "Part one: " RESULT-DISPLAY-1.
  DISPLAY "Part two: " RESULT-DISPLAY-2.
